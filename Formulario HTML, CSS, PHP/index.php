<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="index.css">
    <title>Formulario</title>
</head>

<body>
    <div class="contenedor">
        <form action="validacion.php" method="post">
            <h2 class="titulo">Login</h2>
            <div>
                <label for="email">Correo</label>
                <input type="email" name="email" id="email">
            </div>
            <div>
                <label for="password">Contraseña</label>
                <input type="password" name="password" id="password">
            </div>
            <button type="submit">Iniciar sesión</button>
        </form>
    </div>
</body>

</html>